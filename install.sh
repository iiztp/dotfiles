#!bin/bash/

# Change to unstable mirror
echo -e "deb http://miroir.univ-lorraine.fr/debian/ unstable main non-free-firmware contrib\ndeb-src http://miroir.univ-lorraine.fr/debian/ unstable main non-free-firmware contrib" | sudo tee /etc/apt/sources.list

sudo apt update && sudo apt dist-upgrade
sudo apt autoremove
sudo apt install curl gnupg2

# Spotify
curl -sS https://download.spotify.com/debian/pubkey_6224F9941A8AA6D1.gpg | sudo gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo cp spotify.desktop /usr/share/applications

# Codium
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list

sudo apt update
sudo apt install git sway swaylock zsh firefox notification-daemon libatomic1 libnotify4 libssl-dev xdg-utils unzip zip grimshot pulseaudio brightnessctl waybar meson cmake pkg-config libwayland-dev wayland-protocols libxkbcommon-dev libcairo-dev libpam0g-dev libgdk-pixbuf-2.0-0 curl spotify-client qterminal zsh-autosuggestions fonts-firacode thunar sway-notification-center autoconf automake flex bison check libpango1.0-dev libgdk-pixbuf-2.0-dev libxcb-util-dev libxcb-xkb-dev libxkbcommon-x11-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-randr0-dev libxcb-cursor-dev libxcb-xinerama0-dev libstartup-notification0-dev xdg-desktop-portal xdg-desktop-portal-wlr neovim python3-gi playerctl libplayerctl-dev network-manager codium xwayland wireshark

#
# Network
#
echo -e "source /etc/network/interfaces.d/*\nauto lo\niface lo inet loopback" | sudo tee /etc/network/interfaces

sudo systemctl stop wpa_supplicant
sudo systemctl stop NetworkManager
sudo systemctl start wpa_supplicant
sudo systemctl start NetworkManager

echo "Waiting for connection to restore, or ctrl+c there and reconnect manually"
while [ $(nmcli d | grep -e "wifi" -e "ethernet"  | grep -cw "connected") -eq 0 ]; do
sleep 0.1
done
#
# Greeter
#
sudo apt --no-install-recommends install sddm qml-module-qtquick-layouts qml-module-qtgraphicaleffects qml-module-qtquick-controls2 libqt5svg5 kde-config-sddm
sudo mkdir -p /usr/share/sddm/themes
# https://store.kde.org/p/1312658/
sudo tar -xzvf sugar-candy.tar.gz -C /usr/share/sddm/themes
echo -e "[Theme]\nCurrent=sugar-candy" | sudo tee /etc/sddm.conf

#
# Zsh
#
echo "Installing zsh"
echo "After installation, run this command 'autoload -Uz compinit promptinit && exit' in the zsh that will be launched"
sleep 10

chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k"
git clone https://github.com/zsh-users/zsh-autosuggestions "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions"
cp zsh/.zshrc "$HOME/"
cp zsh/.p10k.zsh "$HOME/"

#
# Config creation
#
config="$HOME/.config"
mkdir "$config"

# Waybar
sudo usermod -a -G input "$USER"
cp -r waybar/ "$config/"
chmod +x "$config/waybar/mediaplayer.py"
chmod +x "$config/waybar/wifi-menu.sh"
chmod +x "$config/waybar/shutdown.sh"
chmod +x "$config/waybar/bt-ss.sh"

# Qterminal
mkdir "$config/qterminal.org/"
cp qterminal/qterminal* "$config/qterminal.org"
sudo cp qterminal/*.colorscheme /usr/share/qtermwidget5/color-schemes/

# Rofi
wget https://github.com/lbonn/rofi/releases/download/1.7.5%2Bwayland3/rofi-1.7.5+wayland3.tar.gz
tar -xvzf rofi-1.7.5+wayland3.tar.gz
cd rofi-1.7.5+wayland3
meson setup build/
ninja -C build/
sudo ninja -C build/ install
cd ..
rm -rf rofi-1.7.5+wayland3*

git clone https://github.com/lr-tech/rofi-themes-collection.git
sudo mkdir -p /usr/share/rofi/themes/
sudo cp rofi-themes-collection/themes/* /usr/share/rofi/themes/
rm -rf rofi-themes-collection

cp -r rofi/ "$config/"

# Swaylock
wget https://github.com/mortie/swaylock-effects/archive/refs/tags/v1.6-4.zip
unzip v1.6-4.zip

cd swaylock-effects-1.6-4
meson build/
ninja -C build/
sudo ninja -C build/ install
cd ..

rm -rf swaylock-effects-1.6-4/ v1.6-4.zip

# Gtk
mkdir "$config/gtk-3.0"
echo -e "[Settings]\ngtk-application-prefer-dark-theme=1" > "$config/gtk-3.0/settings.ini"


# Sway
cp -r sway/ "$config"
# Activate screenshare
echo "exec systemctl --user import-environment DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP" | sudo tee /etc/sway/config.d/50-systemd-user.conf
echo "exec hash dbus-update-activation-environment 2>/dev/null && \\\\ndbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP XDG_CURRENT_DESKTOP=sway" | sudo tee -a /etc/sway/config.d/50-systemd-user.conf

#
# Fonts
#
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
sudo mkdir /usr/share/fonts/truetype/MesloLGS
sudo mv *.ttf /usr/share/fonts/truetype/MesloLGS

# Font-awesome
wget https://github.com/eliyantosarage/font-awesome-pro/releases/download/6.5.1/fontawesome-pro-6.5.1-desktop.zip
unzip fontawesome-pro-6.5.1-desktop.zip

sudo mkdir /usr/share/fonts/opentype
sudo mv fontawesome-pro-6.5.1-desktop/otfs /usr/share/fonts/opentype
sudo fc-cache -f -v

rm -rf fontawesome-pro-6.5.1-desktop*

# Request reboot
systemctl reboot
