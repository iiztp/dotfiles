#!/bin/bash
# Inspired by https://github.com/nickclyde/rofi-bluetooth/blob/master/rofi-bluetooth

if bluetoothctl show | grep -q "Powered: yes"; then
    bluetoothctl power off
else
    bluetoothctl power on
fi
