#!/bin/bash

wifi_scan=$(nmcli --fields "SSID,RATE,SIGNAL,SECURITY" device wifi list | sed 1d)

# Get only one security and names
not_connected=$(echo "$wifi_scan" | grep -v "*" | grep -v "\-\-" | awk '{printf("%s %s %s %s ",$1,$2,$3,$4); if($6 == ""){print $5}else{print $6}}')

# Sort by unique ssid
unique_ssid=$(echo "$not_connected" | awk '!seen[$1]++')

# Sec to emoji
sec_ssid=$(echo "$unique_ssid" | awk '{if ($NF ~ /^W/) print " (",$5,")\t", $1,"\t",$2,$3,"\t",$4; else print " ("$5")\t", $1,"\t",$2,$3,"\t",$4 }')

# Sign to emoji
sign_ssid=$(echo "$sec_ssid" | awk -F'\t' '{printf("%s", substr($0, 1, length($0)-length($NF)-1)); if ($NF >= 0 && $NF < 25) print "\t"; else if ($NF >= 25 && $NF < 50) print "\t"; else if ($NF >= 50 && $NF < 75) print "\t"; else if ($NF >= 75 && $NF <= 100) print "\t"; }')

# Format array
formatted=$(echo "$sign_ssid" | awk -F'\t' '{ printf "%s %s %s %s %s\n", $1, $2, $3, $4, $5 }')

# Get connected
connected=$(nmcli d | grep -e "wifi" -e "ethernet" | grep -w "connected")

firstRow=""
# If we are connected
if [[ ! -z "$connected" ]]; then
    is_ethernet=$(echo "$connected" | grep "ethernet")
    if [[ ! -z "$is_ethernet" ]]; then
        cable=$(echo "$is_ethernet" | awk '{$1=""; $2=""; $3=""; sub("   ", ""); print}')
        firstRow="$firstRow\t  Disconnect from $cable\n"
    fi
    is_wifi=$(echo "$connected" | grep "wifi")
    if [[ ! -z "$is_wifi" ]]; then
        connected_ssid=$(echo "$is_wifi" | awk '{print $4}')
        firstRow="$firstRow\t  Disconnect from $connected_ssid\n"
    fi
fi

# Answer from rofi
chosen_network=$(echo -e "$firstRow$formatted" | rofi -dmenu -i -selected-row 1 -p "Wi-Fi SSID: " -config "$HOME/.config/waybar/wifi-theme.rasi")

message="An error occured while connecting to network"

# If disconnection
if [[ $(echo "$chosen_network" | grep -c "Disconnect from ") -eq 1 ]]; then
    connection=$(echo "$chosen_network" | awk '{print $NF}')
    nmcli con down id "$connection"
    message=" You've been disconnected from $connection"
else
    con_id=$(echo "$chosen_network" | awk '{print $5}')
    if [[ $(nmcli -g NAME connection | grep -c "$con_id") -eq 1 ]]; then
        nmcli connection up id "$con_id" | grep "successfully" && message="Connection established to $con_id"
    else
        if [[ "$chosen_network" =~ "" ]]; then
            password=$(rofi -dmenu -p "Password: ")
        fi
	nmcli device wifi connect "$con_id" password "$password" | grep "successfully" && message="Connection established to $con_id"
    fi
fi

if [[ ! -z "$chosen_network" ]]; then
notify-send "Network Manager" "$message"
fi
