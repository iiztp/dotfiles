#!/bin/bash

# Power menu script using rofi
lock="Lock\t\t\t\t\t       "
shutdown="Shutdown\t\t\t\t       "
reboot="Reboot\t\t\t\t       "
logout="Log Out\t\t\t\t       " 

CHOSEN=$(printf "$lock\n$reboot\n$shutdown\n$logout" | rofi -dmenu -config $HOME/.config/waybar/shutdown-theme.rasi)

if [[ "$CHOSEN" =~ ^Lock.* ]]; then 
  swaylock --clock --indicator --screenshots --effect-scale 0.4 --effect-vignette 0.2:0.5 --indicator-radius 125 --effect-blur 4x2 --datestr "%a %e.%m.%Y" --timestr "%k:%M:%S"
fi
if [[ "$CHOSEN" =~ ^Shutdown.* ]]; then
  systemctl poweroff
fi

if [[ "$CHOSEN" =~ ^Reboot.* ]]; then
  systemctl reboot
fi

if [[ "$CHOSEN" =~ ^Log.* ]]; then
  swaymsg exit
fi
